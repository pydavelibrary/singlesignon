# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('username', models.CharField(unique=True, max_length=25)),
                ('nickname', models.CharField(max_length=25)),
                ('is_lock', models.BooleanField(default=False)),
                ('level', models.IntegerField(choices=[(1, '1'), (80, '80'), (99, 'Root')])),
                ('answer', models.CharField(max_length=50, blank=True)),
                ('login_times', models.IntegerField(default=0)),
                ('ip_admin', models.CharField(max_length=50, blank=True)),
            ],
            options={
                'db_table': 'accounts_user',
            },
        ),
        migrations.CreateModel(
            name='UserToken',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('token', models.CharField(max_length=64)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('expired_on', models.DateTimeField(null=True, blank=True)),
                ('user', models.OneToOneField(related_name='get_user_token', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
