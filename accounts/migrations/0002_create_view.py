# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
        ('admin', '0001_initial')
    ]

    sql = """
    SET FOREIGN_KEY_CHECKS = 0;
    DROP TABLE accounts_user;
    CREATE ALGORITHM=UNDEFINED
    SQL SECURITY DEFINER VIEW `accounts_user` AS
    select
        `admin_id`.`numid` AS `id`,
        `admin_id`.`lock` AS `is_lock`,
        `admin_id`.`user` AS `username`,
        `admin_id`.`pass` AS `password`,
        `admin_id`.`nickname` AS `nickname`,
        `admin_id`.`level` AS `level`,
        `admin_id`.`answer` AS `answer`,
        `admin_id`.`login` AS `login_times`,
        `admin_id`.`ip_admin` AS `ip_admin`,
        `admin_id`.`datetime_admin` AS `last_login`
    from `ton_test2`.`admin_id`;
    SET FOREIGN_KEY_CHECKS = 1;
    """

    operations = [
        migrations.RunSQL(sql)
    ]
