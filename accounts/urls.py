from django.conf.urls import url
# from django.contrib.auth.views import logout
from . import views

urlpatterns = [
    url(r'^login/$', views.login_user, name="accounts.login"),
    # url(r'^register/$', 'register', name="accounts.register"),
    url(r'^logout/$', views.logout_user, name="accounts.logout"),
    url(r'^sso/$', views.sso, name="sso"),
    url(r'^api/$', views.api, name="api"),
]
