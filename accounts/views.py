# -*- coding: utf-8 -*-

from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, resolve_url
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import redirect_to_login
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.conf import settings

from .forms import UserForm
from .models import User, UserToken
from utils.decorators import json_response
import six
from datetime import datetime
from accounts.models import LogsLoginLogoutHistory
import re

@csrf_exempt
def login_user(request):
    logout(request)
    username = password = ''
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                if request.GET.get('next'):
                    apps_name = re.search('apps_name=(\w+)', request.GET.get('next'))
                    if apps_name:
                        apps_name = apps_name.group(1)
                    else:
                        apps_name = None
                else:
                    apps_name = None

                LogsLoginLogoutHistory(username=user.username, nickname=user.nickname, ip_addr=user.ip_admin, created_on=now, types='Login', apps=apps_name).save()
                next_url = request.GET.get('next', '/')
                return HttpResponseRedirect(next_url)
    return render(request, 'login.html')


@csrf_exempt
def logout_user(request):
    if request.user.username:
        if request.GET.get('apps_name'):
            apps_name = request.GET.get('apps_name')
        else:
            apps_name = None
        now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        LogsLoginLogoutHistory(username=request.user.username, nickname=request.user.nickname, ip_addr=request.user.ip_admin, created_on=now, types='Logout', apps=apps_name).save()
    logout(request)
    next_url = request.GET.get('next', '/')
    return HttpResponseRedirect(next_url)


def register(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            form_data = form.save(commit=False)
            user = User.objects.create_user(username=form_data.username, password=form_data.password)
            user = authenticate(username=form_data.username, password=form_data.password)
            login(request, user)
            next_url = request.GET.get('next', '/')
            return HttpResponseRedirect(next_url)
    else:
        form = UserForm()
    data = {
        'form': form,
    }
    return render(request, 'register.html', data)


@login_required
def sso(request):
    def custom_redirect(url, params):
        if six.PY2:
            import urllib
        else:
            import urllib.parse as urllib
        params = urllib.urlencode(params)
        return HttpResponseRedirect(url + "?%s" % params)

    if 'callback_url' not in request.GET:
        return HttpResponse('No callback found.')
    callback_url = request.GET['callback_url']
    if not callback_url.startswith('http://') and not callback_url.startswith('https://'):
        raise Exception('Incorrect redirect URL.')
    next_url = request.GET.get('next_url', None)
    if request.method == 'POST':
        if request.POST.get('confirmation') == 'Yes':
            pass
        else:
            if request.user.username:
                if request.GET.get('apps_name'):
                    apps_name = request.GET.get('apps_name')
                else:
                    apps_name = None
                now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                LogsLoginLogoutHistory(username=request.user.username, nickname=request.user.nickname, ip_addr=request.user.ip_admin, created_on=now, types='Logout', apps=apps_name).save()
            return redirect_to_login(
                request.build_absolute_uri(),
                resolve_url(settings.LOGIN_URL),
                REDIRECT_FIELD_NAME
            )
    elif 'show_confirm' in request.GET:
        data = {
            'callback_url': callback_url,
            'next_url': next_url,
        }
        return render(request, 'confirm_login.html', data)
    token = request.user.generate_token.token
    data = {
        'token': token
    }

    if next_url:
        data['next_url'] = next_url

    return custom_redirect(callback_url, data)


@json_response
def api(request):
    data = {
        'success': False
    }
    token = request.GET.get('token', None)
    if not token:
        return data
    user_token = UserToken.objects.filter(token=token).first()
    if not user_token:
        return data
    user = user_token.user

    data['user'] = user.serialize()
    data['success'] = True
    return data
