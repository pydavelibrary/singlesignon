# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import random
import string

from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.backends import ModelBackend
from django.db import models


class User(AbstractBaseUser):

    USERNAME_FIELD = 'username'

    LEVEL_MEMBER = 1
    LEVEL_80     = 80
    LEVEL_ROOT   = 99

    LEVEL_CHOICES = (
        (LEVEL_MEMBER, '1'),
        (LEVEL_80, '80'),
        (LEVEL_ROOT, 'Root'),
    )

    username       = models.CharField(max_length=25, unique=True)
    nickname       = models.CharField(max_length=25)
    is_lock        = models.BooleanField(default=False)
    level          = models.IntegerField(choices=LEVEL_CHOICES)

    answer         = models.CharField(max_length=50, blank=True)
    login_times    = models.IntegerField(default=0)
    ip_admin       = models.CharField(max_length=50, blank=True)
    # last_login     = models.DateTimeField()

    class Meta:
        db_table = 'accounts_user'

    @property
    def generate_token(self):
        if hasattr(self, 'get_user_token'):
            return self.get_user_token
        else:
            user_token = UserToken(user=self)
            user_token.save()
            return user_token

    def serialize(self):
        last_login = self.last_login.strftime('%Y-%m-%dT%H:%M:%S') if self.last_login else None
        return {
            'id': self.pk,
            'username': self.username,
            'nickname': self.nickname,
            'is_lock': self.is_lock,
            'level': self.level,
            'answer': self.answer,
            'login_times': self.login_times,
            'ip_admin': self.ip_admin,
            'last_login': last_login,
        }


class UserToken(models.Model):

    user = models.OneToOneField(User, related_name='get_user_token')
    token = models.CharField(max_length=64)
    created_on = models.DateTimeField(auto_now_add=True)
    expired_on = models.DateTimeField(null=True, blank=True)

    def generate_token(self):
        length = 64
        return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(length))

    def save(self, *args, **kwargs):
        if not self.pk and not self.token:
            self.token = self.generate_token()
        super(UserToken, self).save(*args, **kwargs)


class UserBackend(ModelBackend):

    def authenticate(self, username=None, password=None):
        user = User.objects.filter(username=username, password=password).first()
        return user


class LogsLoginLogoutHistory(models.Model):

    id                      = models.AutoField(primary_key=True)
    username                = models.CharField(max_length=255, blank=False, null=False)
    nickname                = models.CharField(max_length=255, blank=False, null=False)
    ip_addr                 = models.CharField(max_length=50, blank=False, null=False)
    created_on              = models.DateTimeField(blank=False, null=False)
    types                   = models.CharField(max_length=10, blank=False, null=False)
    apps                    = models.CharField(max_length=255, blank=True, null=True)