from .common_settings import *


INSTALLED_APPS += [
    'accounts',
    'singlesignon',
]

AUTH_USER_MODEL = 'accounts.User'
AUTHENTICATION_BACKENDS = ('accounts.models.UserBackend',)

try:
    from configs import *
except ImportError:
    raise
