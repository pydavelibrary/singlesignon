# -*- coding: utf-8 -*-
from django.shortcuts import render


def index(request):
    data = {}
    if request.user.is_authenticated():
        return render(request, 'index_auth.html', data)
    else:
        return render(request, 'index_noauth.html', data)
