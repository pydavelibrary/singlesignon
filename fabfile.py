from fabric.api import *


env.hosts = ['188.166.243.3:12102']
env.user  = 'root'


def deploy():
    with cd('~/singlesignon'):
        run('git pull')
        run('git submodule update')
        run('python manage.py migrate')
